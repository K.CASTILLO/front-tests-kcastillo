export default (await import('vue')).defineComponent({
name: 'AppView',
components: {
Alert
},
data() {
return {
CountFavoritos: 0
};
},
created() {
this.CountFavoritos = localStorage.getItem('SumFavoritos'); //this.$parent.numfavoritos;
console.log(this.CountFavoritos);
},
computed: {
itemsFromLocalStorage() {
return JSON.parse(localStorage.getItem('favoritos') || '[]');
//return localStorage.getItem('favoritos') || '[]';
},
numfavoritos() {
return localStorage.getItem('SumFavoritos');
}
}
});
