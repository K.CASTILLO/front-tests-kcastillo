#### BUSCADOR GIFS

### DESCRIPCION
Aplicación en la cual buscas por nombre los Gifs, puedes agregarlos a una lista de favoritos que seran almacenados localmente.
La aplicacion consume el Api GIPHY ver documentación https://developers.giphy.com/ y la llaves necesaias se encuentran en .env


### INSTALACION

## Clonar el reposiotorio

## ingresar al directorio del proyecto
cd perritos

## Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production

# Apuntar   llaves de produccion
BASE_URL = http://localhost:8080/
KEY_GIPHY = butMkL0eraB3ksrDpv26swdpkrEn1XI3

```
npm run build
```

### Lints and fixes files
```
npm run lint
```

#### AYUDA
Si no se carga el estilo correctamente seguir estos pasos
### installar tailwindcss

npm install tailwindcss@latest postcss@latest autoprefixer@latest

### agregar tailwindcss
vue add tailwindcss


#### ESTRUCTURA DEL PROYECTO

|-- src
|   |-- assets
|   |-- components
        |-- AlertaComponet.vue
        |-- ListGifComponet.vue
|   |-- views
        |-- BuscadorView.vue
        |-- FavoritosView.vue
|   |-- App.vue
|   |-- main.js
|-- public
|-- README.md


#### ESTADO DEL PROYECTO
Creado 17/01/2024

Proyecto en revision 

### SITIO DE PRODUCCION PRUEBAS
https://gifs.code-innovation.com



### SALUDOS KCASTILLO
