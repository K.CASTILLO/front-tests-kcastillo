#### Admiistrador de colaboradores

### DESCRIPCION
Aplicación en la cual se Muestran los Colaboradores existentes en una tabla con paginación implementada, se puede buscar un colaborador por su nombre ingresando en el input algún nombre y dando ENTER pra desplegar la busqueda.
Se puede Crear un nuevo colaborador dando clic en el botón Crear colaborador e ingresando los datos en el modal desplegado y dando clic en el boton Crear colaborador.
Se puede Editar el colaborador seleccionado dede la tabla -> acciones y seleccionando la opcion Editar.

La aplicacion consume el Api https://dummyjson.com/users ver documentación.


### INSTALACION

## Clonar el reposiotorio

## ingresar al directorio del proyecto
cd formulario

## Project setup
```
npm install
```
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production

# Apuntar  llaves de produccion

BASE_URL = colaboradores.code-innovation.com

VUE_APP_USRAPI = https://dummyjson.com/users

```
npm run build
```

### Lints and fixes files
```
npm run lint
```

#### AYUDA
Si no se carga el estilo correctamente seguir estos pasos
### installar tailwindcss

npm install tailwindcss@latest postcss@latest autoprefixer@latest

### agregar tailwindcss
vue add tailwindcss


#### ESTRUCTURA DEL PROYECTO

|-- src
|   |-- assets
|   |-- components
        |-- AlertComponet.vue
        |-- FormulariofComponet.vue
        |-- TableComponet.vue
|   |-- views
        |-- HomeView.vue
|   |-- App.vue
|   |-- main.js
|-- public
|-- README.md


#### ESTADO DEL PROYECTO
Creado 18/01/2024

Proyecto en revision 

### SITIO DE PRODUCCION PRUEBAS
https://colaboradores.code-innovation.com



### SALUDOS KCASTILLO
